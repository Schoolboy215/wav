//
//  ContainerViewController.swift
//  wav
//
//  Created by Kaylie DeNuto on 2/24/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import UIKit

enum SlideOutState {
    case BothCollapsed
    case LeftPanelExpanded
    case RightPanelExpanded
}

//contains MapController and sidePanelViewController in order to allow slide out menu
class ContainerViewController: UIViewController, CenterViewControllerDelegate {
    
    //map view is classified as center view controller
    var centerNavigationController: UINavigationController!
    var centerViewController: MapController!
    
    var currentState: SlideOutState = .BothCollapsed
    
    //side out panel is classified as left view controller
    var leftViewController: SidePanelViewController?
    
    var cellDelegate : LayerToggleDelegate!
    
    //offset of how far the left panel will push thec center panel to the right
    let centerPanelExpandedOffset: CGFloat = 500
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        centerViewController = UIStoryboard.centerViewController()
        centerViewController.delegate = self
        cellDelegate = centerViewController
        
        // wrap the centerViewController in a navigation controller, so we can push views to it
        // and display bar button items in the navigation bar
        centerNavigationController = UINavigationController(rootViewController: centerViewController)
        view.addSubview(centerNavigationController.view)
        addChildViewController(centerNavigationController)
        
        centerNavigationController.didMoveToParentViewController(self)
        
        //add tap gesture recognizer so side panel will dissapear when the center view controller (map) is tapped
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "handleTapGesture:")
        centerNavigationController.view.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    // MARK: CenterViewController delegate method
    // If the left panel is not already expanded, a sidePanelViewController will be added and then animated onto the screen
    
    func toggleLeftPanel() {
        let notAlreadyExpanded = (currentState != .LeftPanelExpanded)
        
        if notAlreadyExpanded {
            addLeftPanelViewController()
        }
        
        animateLeftPanel(shouldExpand: notAlreadyExpanded)
    }
    
    // Adds a sidePanelViewController as a child of the container if the controller does not already exist
    func addLeftPanelViewController() {
        if (leftViewController == nil) {
            leftViewController = UIStoryboard.leftViewController()
            
            addChildSidePanelController(leftViewController!)
        }
    }
    
    // Works with addLeftPanelViewController() to create the sidePanelViewController
    func addChildSidePanelController(sidePanelController: SidePanelViewController) {
        sidePanelController.cellDelegate = cellDelegate
        
        view.insertSubview(sidePanelController.view, atIndex: 0)
        
        addChildViewController(sidePanelController)
        sidePanelController.didMoveToParentViewController(self)
    }
    
    //slides the left panel in or out based on it's expansion state
    func animateLeftPanel(#shouldExpand: Bool) {
        if (shouldExpand) {
            currentState = .LeftPanelExpanded
            
            animateCenterPanelXPosition(targetPosition: CGRectGetWidth(centerNavigationController.view.frame) - centerPanelExpandedOffset)
        } else {
            animateCenterPanelXPosition(targetPosition: 0) { finished in
                self.currentState = .BothCollapsed
                
                //self.leftViewController!.view.removeFromSuperview()
                //self.leftViewController = nil;
            }
        }
    }
    
    //slides center view controller to the left or right so the side panel is visible
    func animateCenterPanelXPosition(#targetPosition: CGFloat, completion: ((Bool) -> Void)! = nil) {
        UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .CurveEaseInOut, animations: {
            self.centerNavigationController.view.frame.origin.x = targetPosition
            }, completion: completion)
    }
    
    // MARK: Gesture recognizer
    func handleTapGesture(recognizer: UITapGestureRecognizer) {
        if(currentState == .LeftPanelExpanded){
            toggleLeftPanel()
        }
        
    }

}

private extension UIStoryboard {
    class func mainStoryboard() -> UIStoryboard { return UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()) }
    
    class func leftViewController() -> SidePanelViewController? {
        return mainStoryboard().instantiateViewControllerWithIdentifier("LeftViewController") as? SidePanelViewController
    }
    
    
    class func centerViewController() -> MapController? {
        return mainStoryboard().instantiateViewControllerWithIdentifier("MapViewController") as? MapController
    }
}
