//
//  ViewController.swift
//  wav
//
//  Created by James McKay on 1/25/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {
    
    @IBOutlet var descriptionField : UITextView!
    @IBOutlet weak var myPicker : UIPickerView!
    let typeChoices = ["shallowWater","floatingDebris","fishCaught","coastGuard","childrenPlaying","partyTime"]
    
    var selectedType : String = ""
    
    
    @IBAction func submitTapped(sender : AnyObject) {
        
        var description = descriptionField.text as String
        
        var currentLocation : CLLocation!
        
        if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.AuthorizedWhenInUse){
                
                //currentLocation = locManager.location
                //descriptionField.text = "\(currentLocation.coordinate.longitude)"
                
        }
        
        var object = PFObject(className: "wavAlert")
        object["type"] = selectedType
        object["description"] = description
        //object["location"] = PFGeoPoint(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        object.save()
    }
    
    @IBAction func getPoints(){
        //returnParseObjects()
    }
    
    @IBAction func unwindToList(segue: UIStoryboardSegue) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if ((myPicker) != nil) {
            myPicker.dataSource = self
            myPicker.delegate = self
        }
        
        
        selectedType = typeChoices[0]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Delegates and data sources
    //MARK: Data Sources
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return typeChoices.count
    }
    //MARK: Delegates
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return typeChoices[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedType = typeChoices[row]
    }
}

