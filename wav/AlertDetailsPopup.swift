//
//  AlertDetailsPopup.swift
//  wav
//
//  Created by James McKay on 4/13/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import UIKit

class AlertDetailsPopup: UIViewController {
    
    @IBOutlet var titleLabel : UILabel!
    @IBOutlet var descriptionLabel : UILabel!
    @IBOutlet var kindImage : UIImageView!
    @IBOutlet var usernameLabel : UILabel!
    @IBOutlet var expiryLabel : UILabel!
    
    var parseID : String!
    var anno : AlertAnnotation!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func verifyPressed(){
        addTimeToAlert(&anno!, 30)
        dismissViewControllerAnimated(true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
