//
//  MapController.swift
//  wav
//
//  Created by David Goehring on 2/18/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import UIKit
import MapKit

@objc
protocol CenterViewControllerDelegate {
    optional func toggleLeftPanel()
    optional func collapseSidePanels()
}

func imageResize (imageObj:UIImage, sizeChange:CGSize)-> UIImage{
    
    let hasAlpha = false
    let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
    
    UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
    imageObj.drawInRect(CGRect(origin: CGPointZero, size: sizeChange))
    
    let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
    return scaledImage
}

class MapController: UIViewController, ProfileDelegate, MKMapViewDelegate, LayerToggleDelegate, nearbyAlertsHandler, alertChoicesDelegate, CLLocationManagerDelegate, nearbyUserHandler {
    
    @IBOutlet weak var mapView : MKMapView!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var layerButton: UIButton!
    @IBOutlet weak var alertButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    var weatherLabel: UILabel = UILabel()
    
    //@IBOutlet weak var weatherLabel: UILabel!
    var delegate: CenterViewControllerDelegate?
    var compassState = 0
    var weatherLayerState = 0
    var POILayerState = true
    var POIWasOn = true
    var weatherCircle: MKCircle?
    
    var nearbyAlerts : [AnyObject]! = []
    var nearbyUsers : [AnyObject]! = []
    
    var locationManager = CLLocationManager()
    
    var latitude = CLLocationDegrees(0)
    var longitude = CLLocationDegrees(0)
    
    var angle = 0.0;
    
    var webView : UIWebView?
    
    var timer : NSTimer!
    var startedTimer : Bool = false
    
    var mapRect = CGRect()
    var mapLastRegion = MKCoordinateRegion()
    
    func doneChoosing(child: AlertChoicesPopup) {
        if child.lastPressed > -1 && child.handlingHold == true {
            addHoldAlert(child.lastPressed, description: child.descriptionText, location: child.remoteLocation)
            return
        }
        if child.lastPressed > -1 {
            addAlert(child.lastPressed, description: child.descriptionText)
        }
    }
    
    //nearbyAlertsHandler
    func doneLoading() {
        let annotationsToRemove = mapView.annotations.filter { $0 !== self.mapView.userLocation }
        self.mapView.removeAnnotations( annotationsToRemove )
        for object in nearbyAlerts {
            var obj = PFObject(className: "wavAlert")
            obj = object as! PFObject
            var pin = AlertAnnotation()
            if obj["type"] as! NSString == "shallowWater" {
                pin.imageName = "shallow-water"
                pin.kind = "Shallow Water"
            } else if obj["type"] as! NSString == "coastGuard" {
                pin.imageName = "coast-guard"
                pin.kind = "Coast Guard"
            } else if obj["type"] as! NSString == "floatingDebris" {
                pin.imageName = "debris"
                pin.kind = "Floating Debris"
            } else if obj["type"] as! NSString == "fishCaught" {
                pin.imageName = "fishing"
                pin.kind = "Fish Caught"
            } else if obj["type"] as! NSString == "childrenPlaying" {
                pin.imageName = "children"
                pin.kind = "Children Playing"
            } else if obj["type"] as! NSString == "partyTime" {
                pin.imageName = "party"
                pin.kind = "Ongoing Party"
            }
            pin.coordinate = CLLocationCoordinate2D(latitude: obj["location"].latitude, longitude: obj["location"].longitude)
            pin.title = obj["description"] as! String
            pin.descriptionText = obj["description"] as! String
            pin.username = obj["sender"] as! String
            pin.score = obj["score"] as! Int
            pin.internalID = obj.objectId
            pin.expires = obj["expires"] as! NSDate
            mapView.addAnnotation(pin)
        }
    }
    
    //nearbyUserHandler
    func doneGettingNearbyUsers() {
        for object in nearbyUsers {
            var obj = PFObject(className: "wavUsers")
            obj = object as! PFObject
            var pin = UserAnnotation()
            pin.coordinate = CLLocationCoordinate2D(latitude: obj["lastKnownLoc"].latitude, longitude: obj["lastKnownLoc"].longitude)
            pin.imageName = "userIcon"
            pin.username = obj["userName"] as! String
            mapView.addAnnotation(pin)
        }
    }
    //nearbyAlertsHandler
    func addToArray(object: AnyObject) {
        var obj = PFObject(className: "wavAlert")
        obj = object as! PFObject
        let difference = Int((obj["expires"] as! NSDate).timeIntervalSinceDate(NSDate()))
        if difference >= 0 {
            nearbyAlerts.append(object)
        }
    }
    
    //nearbyUserHandler
    func addNearbyUser(object: AnyObject) {
        var obj = PFObject(className: "wavUsers")
        obj = object as! PFObject
        let difference = Int(NSDate().timeIntervalSinceDate(obj.updatedAt))
        
        if difference < 30 && obj["userName"] as! String != UserProfile.SharedInstance.Username {
            nearbyUsers.append(object)
        }
    }
    
    func addAlert(kind : Int, description : String) {
        var currentLocation = locationManager.location
        
        var object = PFObject(className: "wavAlert")
        var typeNames = ["childrenPlaying","coastGuard","floatingDebris","fishCaught","partyTime","shallowWater"]
        object["type"] = typeNames[kind]
        object["description"] = description
        object["location"] = PFGeoPoint(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        object["sender"] = UserProfile.SharedInstance.Username
        object["score"] = 1
        
        var components = NSDateComponents()
        components.setValue(30, forComponent: NSCalendarUnit.MinuteCalendarUnit);
        let date: NSDate = NSDate()
        var expirationDate = NSCalendar.currentCalendar().dateByAddingComponents(components, toDate: date, options: NSCalendarOptions(0))
        
        object["expires"] = expirationDate
        object.save()
        
        
        let annotationsToRemove = mapView.annotations.filter { $0 !== self.mapView.userLocation }
        mapView.removeAnnotations( annotationsToRemove )
        
        nearbyAlerts = []
        if POILayerState {
            var loc = locationManager.location
            returnParseObjects(self, loc)
        }
        
    }
    
    func addHoldAlert(kind : Int, description : String, location : CLLocationCoordinate2D) {
        var currentLocation = locationManager.location
        
        var object = PFObject(className: "wavAlert")
        var typeNames = ["childrenPlaying","coastGuard","floatingDebris","fishCaught","partyTime","shallowWater"]
        object["type"] = typeNames[kind]
        object["description"] = description
        object["location"] = PFGeoPoint(latitude: location.latitude, longitude: location.longitude)
        object["sender"] = UserProfile.SharedInstance.Username
        object["score"] = 1
        
        var components = NSDateComponents()
        components.setValue(30, forComponent: NSCalendarUnit.MinuteCalendarUnit);
        let date: NSDate = NSDate()
        var expirationDate = NSCalendar.currentCalendar().dateByAddingComponents(components, toDate: date, options: NSCalendarOptions(0))
        
        object["expires"] = expirationDate
        object.save()
        
        
        let annotationsToRemove = mapView.annotations.filter { $0 !== self.mapView.userLocation }
        mapView.removeAnnotations( annotationsToRemove )
        
        nearbyAlerts = []
        if POILayerState {
            var loc = locationManager.location
            returnParseObjects(self, loc)
        }
        
    }
    
    func action(sender:UIGestureRecognizer) {
        if (sender.state == UIGestureRecognizerState.Ended || sender.state == UIGestureRecognizerState.Changed) {
            return
        }
        var point = sender.locationInView(self.mapView)
        var locCoord = self.mapView.convertPoint(point, toCoordinateFromView: self.mapView)
        
        //NEW
        var popoverContent = self.storyboard?.instantiateViewControllerWithIdentifier("alertChoicesPopup") as! AlertChoicesPopup
        popoverContent.modalPresentationStyle = UIModalPresentationStyle.Popover
        var popover = popoverContent.popoverPresentationController
        popoverContent.preferredContentSize = CGSizeMake(210,320)
        popover?.sourceView = view
        popover?.sourceRect = CGRectMake(point.x,point.y,0,0)
        
        var anView = MKAnnotationView()
        
        popoverContent.delegate = self
        popoverContent.handlingHold = true
        popoverContent.remoteLocation = locCoord
        self.presentViewController(popoverContent, animated: true, completion: nil)
        //END OF NEW
        
        
        /*var object = PFObject(className: "wavAlert")
        object["type"] = "shallowWater"
        object["description"] = "Added from hold"
        object["location"] = PFGeoPoint(latitude: locCoord.latitude, longitude: locCoord.longitude)
        object.save()
        
        
        let annotationsToRemove = mapView.annotations.filter { $0 !== self.mapView.userLocation }
        mapView.removeAnnotations( annotationsToRemove )
        nearbyAlerts = []
        if POILayerState  {
//<<<<<<< HEAD
            var loc = locationManager.location
            returnParseObjects(self, loc)
        }
=======
            returnParseObjects(self, locationManager)
        }
>>>>>>> c26a1426d598947b58ef1ab25f7dfe964125fd6a*/
        
    }
    
    func mapView(mapView: MKMapView!, didSelectAnnotationView view: MKAnnotationView!) {
        if !(view.annotation is AlertAnnotation){
            return
        }
        let anno = view.annotation as! AlertAnnotation
        
        mapView.deselectAnnotation(view.annotation, animated: true)
        
        var popoverContent = self.storyboard?.instantiateViewControllerWithIdentifier("alertDetails") as! AlertDetailsPopup
        popoverContent.modalPresentationStyle = UIModalPresentationStyle.Popover
        var popover = popoverContent.popoverPresentationController
        popoverContent.preferredContentSize = CGSizeMake(450,150)
        popover?.sourceView = view
        popover?.sourceRect = CGRectMake(20,20,0,0)
        
        var anView = MKAnnotationView()
        
        self.presentViewController(popoverContent, animated: true, completion: nil)
        popoverContent.titleLabel.text = anno.kind
        popoverContent.descriptionLabel.text = anno.descriptionText
        popoverContent.kindImage.image = UIImage(named: anno.imageName)
        popoverContent.usernameLabel.text = "User : \(anno.username)"
        popoverContent.parseID = anno.internalID
        popoverContent.anno = anno
        let difference = Int(anno.expires.timeIntervalSinceDate(NSDate()))
        switch difference {
        case 0:
            popoverContent.expiryLabel.text = "Expires in less than a minute"
        case 1:
            popoverContent.expiryLabel.text = "Expires in one minutes"
        default:
            popoverContent.expiryLabel.text = "Expires in \(difference/60) minutes"
        }
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if !(annotation is AlertAnnotation) && !(annotation is UserAnnotation) {
            return nil
        }
        
        let reuseId = "teschange"
        var anView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
        
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView.canShowCallout = true
        }
        else {
            anView.annotation = annotation
        }
        
        //Set annotation-specific properties **AFTER**
        //the view is dequeued or created...
        if (annotation is AlertAnnotation){
            let cpa = annotation as! AlertAnnotation
            anView.image = UIImage(named:cpa.imageName)
        } else {
            let cpa = annotation as! UserAnnotation
            anView.image = UIImage(named:cpa.imageName)
        }
        
        anView.image = imageResize(anView.image, CGSize(width: 35, height: 35))

        return anView
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        var lpgr = UILongPressGestureRecognizer(target: self, action: "action:")
        lpgr.minimumPressDuration = 2.0;
        self.view.addGestureRecognizer(lpgr)
        
        var global : UserProfile = UserProfile.SharedInstance
        
        navigationController?.setNavigationBarHidden(navigationController?.navigationBarHidden == false, animated: true)
        //var weatherLabel: UILabel = UILabel()
        
        weatherLabel.frame = CGRectMake(250, 50, 300, 100)
        weatherLabel.hidden = true
        weatherLabel.numberOfLines = 0
        weatherLabel.layer.cornerRadius = 10
        weatherLabel.clipsToBounds = true
        weatherLabel.textAlignment = .Center;
        weatherLabel.textColor = UIColor.whiteColor()
        weatherLabel.backgroundColor = UIColor.grayColor()
        weatherLabel.alpha = 0.85
        self.view.addSubview(weatherLabel)
        
        
        mapView.delegate = self
        
        var currentLocation : CLLocation!
        if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.AuthorizedWhenInUse){
            
            
        } else {
            locationManager.requestAlwaysAuthorization()
        }
        
        
        currentLocation = locationManager.location
        let loc = CLLocationCoordinate2D(latitude: locationManager.location.coordinate.latitude, longitude: locationManager.location.coordinate.longitude)
        let span = MKCoordinateSpanMake(1 , 1)
        let reg = MKCoordinateRegionMake(loc, span)
        mapView.setRegion(reg, animated: false)
       
        
        // display user's location with a blue dot
        mapView.showsUserLocation = true;
        if POILayerState {
            var loc = locationManager.location
            returnParseObjects(self, loc)
        }
        
        var centerLocation = mapView.centerCoordinate
        setUpCompass()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if !startedTimer {
            self.timer?.invalidate()
            self.timer = NSTimer.scheduledTimerWithTimeInterval(15.0, target: self, selector: "tick", userInfo: nil, repeats: true)
            startedTimer = true
        }
    }
    
    func tick(){
        switch POILayerState {
        case true:
            nearbyAlerts = []
            var loc = locationManager.location
            returnParseObjects(self, loc)
        default:
            nearbyAlerts = []
            let annotationsToRemove = mapView.annotations.filter { $0 !== self.mapView.userLocation }
            self.mapView.removeAnnotations( annotationsToRemove )
            return
        }
        
        if UserProfile.SharedInstance.VisibilityState {
            nearbyUsers = []
            var loc = locationManager.location
            updateUserLocation(loc)
            getNearbyUsers(self,loc)
        }
    }
    
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        var weatherCircleRenderer = MKCircleRenderer(overlay: overlay)
        weatherCircleRenderer.fillColor = UIColor.blueColor()
        weatherCircleRenderer.strokeColor = UIColor.blackColor()
        weatherCircleRenderer.alpha = 0.5
        weatherCircleRenderer.lineWidth = 1
        return weatherCircleRenderer
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        NSLog("Catching segue")
        if(segue.identifier == "ProfileSegue") {
            let profileView = segue.destinationViewController as! ProfileController
            profileView.delegate = self
            syncProfile()
        }
        else if  (segue.identifier == "alertChoicesSegue") {
            if let controller = segue.destinationViewController as? AlertChoicesPopup {
                controller.mainView = self
                controller.delegate = self
            }
        }
    }
    //ProfileDelegate
    func done(child: ProfileController) {
        NSLog("Returned from Profile View")
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    //LayerToggleDelegate
    func toggleLayer(layer: String, state: Bool) {
        switch layer {
            case "Points of Interest":
                POILayerState = state
                switch state {
                case false:
                    nearbyAlerts = []
                    let annotationsToRemove = mapView.annotations.filter { $0 !== self.mapView.userLocation }
                    self.mapView.removeAnnotations( annotationsToRemove )
                    POIWasOn = false
                default:
                    var loc = locationManager.location
                    returnParseObjects(self, loc)
                    POIWasOn = true
                }
                break
            case "NOAA Data":
                if state {
                    if POILayerState{
                        POILayerState = false
                    }
                    timer.invalidate()
                    startedTimer = false
                    unloadMap()
                    startWebView()
                    
                }
                else {
                    if POIWasOn {
                        POILayerState = true
                    }
                    removeWebView()
                    loadNewMap()
                    timer = NSTimer.scheduledTimerWithTimeInterval(15.0, target: self, selector: "tick", userInfo: nil, repeats: true)
                    startedTimer = true

                }
                break
            case "ARGUS Data":
                break
            case "Satellite":
                if state {
                    mapView.mapType = MKMapType.Satellite
                }
                else {
                    mapView.mapType = MKMapType.Standard
                }
            case "Weather":
                if state {
                    weatherLayerState = 1
                    updateWeather()
                    if weatherCircle != nil {
                        mapView.removeOverlay(weatherCircle)
                    }
                    var centerLocation = mapView.centerCoordinate
                    weatherCircle = MKCircle(centerCoordinate: centerLocation, radius: 1000)
                    mapView.addOverlay(weatherCircle)
                }
                else{
                    weatherLayerState = 0
                    weatherLabel.hidden = true
                    mapView.removeOverlay(self.weatherCircle)
                }
            default:
                mapView.mapType = mapView.mapType
        }

    }
    
    func updateWeather() {
        if weatherLayerState == 1 {
            var centerLocation = mapView.centerCoordinate
            weatherURL = "http://api.openweathermap.org/data/2.5/weather?lat=\(centerLocation.latitude)&lon=\(centerLocation.longitude)&units=imperial"
            WeatherData.getDataFromOpenWeatherWithSuccess { (weatherData) -> Void in
                let json = JSON(data: weatherData)
                var temp = json["main"]["temp"].floatValue
                var wind = json["wind"]["speed"].floatValue
                var pressure = json["main"]["pressure"].floatValue
                var windDegrees = json["wind"]["deg"].floatValue
                NSOperationQueue.mainQueue().addOperationWithBlock({
                    
                    var weatherString = self.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
                    
                    self.weatherLabel.text = weatherString
                    self.weatherLabel.frame = CGRectMake(250, 50, 300, 100)
                    self.weatherLabel.hidden = false
                    
                    
                })
            }
        }
    }
    
    func updateWeatherLabel(temp: Float, wind: Float, pressure: Float, windDegrees: Float) -> String! {
        var windDirection: String?
        var directions = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"]
        var i:Int = Int((windDegrees + 11.25)/22.5)
        windDirection = directions[i%16]
        
        var temptemp = Int(temp*10)
        
        var weatherString = "Temperature: \(Float(temptemp)/10.0) F \n Wind: \(Int(wind)) mph \(windDirection!) \n Pressure: \(Int(pressure)) hpa"
        return weatherString
    }
    
    //update weather info when user pans around screen
    func mapView(mapView: MKMapView!, regionDidChangeAnimated animated: Bool) {
        updateWeather()
        if weatherLayerState == 1 {
            mapView.removeOverlay(weatherCircle)
            var centerLocation = mapView.centerCoordinate
            weatherCircle = MKCircle(centerCoordinate: centerLocation, radius: 1000)
            mapView.addOverlay(weatherCircle)
        }
        if compassState == 1 {
            if !(mapView.centerCoordinate.latitude == mapView.userLocation.coordinate.latitude && mapView.centerCoordinate.longitude == mapView.userLocation.coordinate.longitude) {
                compassState = 0
                locationButton.setImage(UIImage(named: "compass-not-centered"), forState: .Normal)
            }
            else {
                locationButton.setImage(UIImage(named: "compass-centered"), forState: .Normal)
            }
        }
    }
    
    func mapView(mapView: MKMapView!, regionWillChangeAnimated animated: Bool) {
        if weatherLayerState == 1 {
            if weatherCircle != nil {
            }
        }
    }
    
    @IBAction func compassButtonTapped(sender:UIButton){
        var currentLocation = locationManager.location
        if compassState == 0{
            //center on user's location
            //change image to location active
            let loc = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
            let span = MKCoordinateSpanMake(1 , 1)
            let reg = MKCoordinateRegionMake(loc, span)
            mapView.setRegion(reg, animated: false)
            locationButton.setImage(UIImage(named: "compass-centered"), forState: .Normal)
            compassState = 1
        }
        else if compassState == 1 {
            //compass style
            //change image to compass active
            compassState = 2
            locationManager.startUpdatingHeading()
            locationButton.setImage(UIImage(named: "compass"), forState: .Normal)
        }
        else if compassState == 2 {
            //return to normal
            //change image back to location inactive
            let loc = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
            let span = MKCoordinateSpanMake(1 , 1)
            let reg = MKCoordinateRegionMake(loc, span)
            mapView.setRegion(reg, animated: false)
            locationButton.setImage(UIImage(named: "compass-centered"), forState: .Normal)
            compassState = 1
            locationManager.stopUpdatingHeading()
        }
    }
    
    @IBAction func slideOutTapped(sender: AnyObject) {
        delegate?.toggleLeftPanel?()
    }

    func alignMaps(){
        var str = "mapCoordinates"
        var results =  webView?.stringByEvaluatingJavaScriptFromString("document.getElementById(\"\(str)\").innerHTML")!
        var lat = Float(0)
        var latM = Float(0)
        var latS = Float(0)
        var long = Float(0)
        var longM = Float(0)
        var longS = Float(0)
        if results?.isEmpty == false{
            var coord = split(results!) {$0 == " "}
            
            
            
            lat = (coord[0].substringToIndex(coord[0].endIndex) as NSString).floatValue
            latM = (coord[1].substringToIndex(coord[1].endIndex) as NSString).floatValue
            latS = (coord[2].substringToIndex(coord[2].endIndex).substringToIndex(coord[2].endIndex) as NSString).floatValue
            long = (coord[3].substringToIndex(coord[3].endIndex) as NSString).floatValue
            longM = (coord[4].substringToIndex(coord[4].endIndex) as NSString).floatValue
            longS = (coord[5].substringToIndex(coord[5].endIndex) as NSString).floatValue
            latitude = Double(lat + latM/60 + latS/360)
            longitude = Double(long + longM/60 + longS/360)
            
            let loc = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let span = MKCoordinateSpanMake(1, 1)
            let reg = MKCoordinateRegionMake(loc, span)
            mapView.setRegion(reg, animated: false)
        }
    }
    
    func startWebView()
    {
        webView = UIWebView(frame: CGRect(x: -100, y: -100, width: 100 + view.frame.height, height: view.frame.height + 200))
        view.addSubview(webView!)
        
        webView!.loadRequest(NSURLRequest(URL: NSURL(string: "http://www.nauticalcharts.noaa.gov/ENCOnline/enconline.html")!))
        view.bringSubviewToFront(layerButton)
        view.bringSubviewToFront(profileButton)
    }
    
    func removeWebView()
    {
        webView?.removeFromSuperview()
        webView = nil
    }
    
    func setUpCompass(){
        locationManager.delegate = self
    }
    
    
    func locationManagerShouldDisplayHeadingCalibration(manager: CLLocationManager!) -> Bool{
        return true
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateHeading newHeading: CLHeading!){
        mapView.camera.heading = manager.heading.trueHeading
    }
    
    func loadNewMap(){
        
        mapView = MKMapView()
        mapView.showsUserLocation = true;
        
        mapView.mapType = .Standard
        mapView.frame = view.frame
        mapView.delegate = self
        view.addSubview(mapView)
        mapView.region = mapLastRegion
        
        var currentLocation : CLLocation!
        currentLocation = locationManager.location
        let loc = CLLocationCoordinate2D(latitude: locationManager.location.coordinate.latitude, longitude: locationManager.location.coordinate.longitude)
        let span = MKCoordinateSpanMake(1 , 1)
        let reg = MKCoordinateRegionMake(loc, span)
        mapView.setRegion(reg, animated: false)
        
        
        // display user's location with a blue dot
        mapView.showsUserLocation = true;
        if POILayerState {
            var loc = locationManager.location
            returnParseObjects(self, loc)
        }
        
        //view.bringSubviewToFront(mapView)
        view.bringSubviewToFront(layerButton)
        view.bringSubviewToFront(profileButton)
        view.bringSubviewToFront(alertButton)
        view.bringSubviewToFront(locationButton)
        view.bringSubviewToFront(weatherLabel)
    }

    func unloadMap(){
        mapRect = mapView.layer.bounds
        mapLastRegion = mapView.region
        mapView.removeFromSuperview()
        
    }
}