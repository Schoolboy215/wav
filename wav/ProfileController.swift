//
//  ProfileController.swift
//  wav
//
//  Created by David Goehring on 2/18/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import UIKit

protocol ProfileDelegate {
    func done(child: ProfileController)
}

struct GlobalConstants {
    static let migrationCodeLength = 6
}

class ProfileController: UIViewController {

    
    var delegate: ProfileDelegate!
    
    //These are placeholders for the actual profile image choices
    //By replacing them with new image names the code should handle the size difference fine
    var images = ["userIcon.png", "shallow-water.png", "profile.png", "fishing.png", "children.png", "coast-guard.png"]
    var imageIndex = 1
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet var invisSwitch : UISwitch!
    @IBOutlet var userNameLabel : UILabel!
    @IBOutlet var scoreLabel : UILabel!
    @IBOutlet var migrationCodeLabel : UILabel!
    @IBOutlet var codeGenerationButton : UIButton!
    
    var migrationCodeAlertController : UIAlertController!
    var existingCodeAlertController : UIAlertController!
    
    
    @IBAction func invisSwitchThrown() {
        UserProfile.SharedInstance.VisibilityState = invisSwitch.on
        setInvisState()
    }
    
    @IBAction func backPress() {
        delegate.done(self)
    }
    override func viewDidLoad() {
        
        if UserProfile.SharedInstance.MigrationCode == "" {
            migrationCodeLabel.text = "No Code Generated"
        }
        else {
            migrationCodeLabel.text = UserProfile.SharedInstance.MigrationCode
        }
        super.viewDidLoad()
        invisSwitch.setOn(UserProfile.SharedInstance.VisibilityState, animated: false)
        userNameLabel.text = UserProfile.SharedInstance.Username
        scoreLabel.text = String(UserProfile.SharedInstance.Score)
        
        loadPic()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func migrationGenerationPress() {
        if UserProfile.SharedInstance.MigrationCode == "" {
            var migrationCode = randomStringWithLength(GlobalConstants.migrationCodeLength)
            migrationCodeAlertController = UIAlertController(title: "Generated Migration Code", message: "Here is your Account Migration Code. Enter this code into the device to which you wish to transfer your WAV account. This code will expire in two hours.\n\n" + (migrationCode as String), preferredStyle: UIAlertControllerStyle.Alert)
            let submitCreationAction = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            migrationCodeAlertController.addAction(submitCreationAction)
            presentViewController(migrationCodeAlertController, animated: true, completion: nil)
            migrationCodeLabel.text = migrationCode as String
            UserProfile.SharedInstance.MigrationCode = migrationCode as String
            migrationCodeCreation(migrationCode as String)
        }
        else {
            existingCodeAlertController = UIAlertController(title: "Code already generated", message: "Please use the existing code or wait for it to expire before generating a new code.", preferredStyle: UIAlertControllerStyle.Alert)
            let submitExistingAction = UIAlertAction(title: "Dismiss", style: .Default, handler: nil)
            existingCodeAlertController.addAction(submitExistingAction)
            presentViewController(existingCodeAlertController, animated: true, completion: nil)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func randomStringWithLength (len : Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        var randomString : NSMutableString = NSMutableString(capacity: len)
        
        for (var i=0; i < len; i++){
            var length = UInt32 (letters.length)
            var rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
        }
        
        return randomString
    }
    
    @IBAction func nextPic(){
        imageIndex = (imageIndex + 1)%images.count
        profileImageView.image = UIImage(named: images[imageIndex])
        savePic()
    }
    
    @IBAction func lastPic(){
        imageIndex = (imageIndex - 1 + images.count)%images.count
        profileImageView.image = UIImage(named: images[imageIndex])
        savePic()
    }
    
    func loadPic(){
        var libpath = NSSearchPathForDirectoriesInDomains(.LibraryDirectory, .UserDomainMask, true)[0] as! NSString
        libpath = libpath.stringByAppendingPathComponent("imageNumber.txt")
        var file = NSData(contentsOfFile: libpath as String)
        if file != nil{
            var temp = NSString(data: file!, encoding: NSUTF8StringEncoding)?.integerValue
            imageIndex = temp!
        }else{
            savePic()
        }
        
        if imageIndex < 0 || imageIndex >= images.count {
            imageIndex = 0
        }
        
        profileImageView.image = UIImage(named: images[imageIndex])
    }

    func savePic(){
        var data = NSString(string: "\(imageIndex)")
        var libpath = NSSearchPathForDirectoriesInDomains(.LibraryDirectory, .UserDomainMask, true)[0] as! NSString
        libpath = libpath.stringByAppendingPathComponent("imageNumber.txt")
        NSLog(libpath as String)
        data.writeToFile(libpath as String, atomically: false, encoding: NSUTF8StringEncoding, error: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
