//
//  AlertChoicesPopup.swift
//  wav
//
//  Created by James McKay on 4/10/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import UIKit

protocol alertChoicesDelegate {
    func doneChoosing(child : AlertChoicesPopup)
}

class AlertChoicesPopup: UIViewController {
    
    var delegate : alertChoicesDelegate!
    var lastPressed = 0
    var handlingHold = false
    var remoteLocation : CLLocationCoordinate2D!
    var descriptionText : String = ""
    
    @IBOutlet var button1 : UIButton!
    @IBOutlet var button2 : UIButton!
    @IBOutlet var button3 : UIButton!
    @IBOutlet var button4 : UIButton!
    @IBOutlet var button5 : UIButton!
    @IBOutlet var button6 : UIButton!
    
    var buttonArray : [UIButton]!
    var kindArray : [String] = ["Children Playing", "Coast Guard", "Debris in Water", "Fish Caught", "Ongoing Party", "Shallow Water"]
    
    var addDescriptionAlert : UIAlertController!
    @IBOutlet weak var descriptionField : UITextField!
    //var invalidSymbolAlert : UIAlertController!
    
    @IBAction func pressedButton(sender : AnyObject) {
        for i in 0...buttonArray.count {
            if sender as! NSObject == buttonArray[i] {
                lastPressed = i
                break
            }
        }
        buildAlertControllers()
        presentViewController(addDescriptionAlert, animated: true, completion: nil)
    }
    
    var mainView : UIViewController!
    
    
    override func awakeFromNib() {
        view.layoutIfNeeded()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonArray = [button1,button2,button3,button4,button5,button6]
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func buildAlertControllers() {
        addDescriptionAlert = UIAlertController(title: "\(kindArray[lastPressed])", message: "Please add a description for your alert.", preferredStyle: UIAlertControllerStyle.Alert)
        descriptionField = UITextField()
        addDescriptionAlert.addTextFieldWithConfigurationHandler(self.addDescriptionTextField)
        
        let submitHandler = { (action:UIAlertAction!) -> Void in
            self.descriptionText = self.descriptionField.text
            self.delegate.doneChoosing(self)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        let cancelHandler = { (action:UIAlertAction!) -> Void in
            self.lastPressed = -1
            self.delegate.doneChoosing(self)
        }
        let cancelAddSymbol = UIAlertAction(title: "Cancel", style: .Default, handler: cancelHandler)
        let submitAddSymbol = UIAlertAction(title: "Submit", style: .Default, handler: submitHandler)
        addDescriptionAlert.addAction(cancelAddSymbol)
        addDescriptionAlert.addAction(submitAddSymbol)
    }
    
    func addDescriptionTextField(textField : UITextField!) {
        textField.placeholder = "Enter Description"
        //textField.autocapitalizationType = UITextAutocapitalizationType.AllCharacters
        self.descriptionField = textField
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */

}
