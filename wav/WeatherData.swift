//
//  DataManager.swift
//  wav
//
//  Created by Kaylie DeNuto on 3/20/15
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import Foundation

var weatherURL: String?

class WeatherData {
    
    class func getDataFromOpenWeatherWithSuccess(success: ((weatherData: NSData!) -> Void)) {
        //1
        loadDataFromURL(NSURL(string: weatherURL!)!, completion:{(data, error) -> Void in
            //2
            if let urlData = data {
                //3
                success(weatherData: urlData)
            }
        })
    }
    
    class func loadDataFromURL(url: NSURL, completion:(data: NSData?, error: NSError?) -> Void) {
        var session = NSURLSession.sharedSession()
        
        // Use NSURLSession to get data from an NSURL
        let loadDataTask = session.dataTaskWithURL(url, completionHandler: { (data: NSData!, response: NSURLResponse!, error: NSError!) -> Void in
            if let responseError = error {
                completion(data: nil, error: responseError)
            } else if let httpResponse = response as? NSHTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    var statusError = NSError(domain:"com.raywenderlich", code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                    completion(data: nil, error: statusError)
                } else {
                    completion(data: data, error: nil)
                }
            }
        })
        
        loadDataTask.resume()
    }
}