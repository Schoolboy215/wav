//
//  SidePanelCellController.swift
//  wav
//
//  Created by David Goehring on 3/6/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import UIKit
import MapKit

protocol LayerToggleDelegate {
    func toggleLayer(layer: String, state: Bool)
}

protocol LayerStateDelegate {
    func updateSavedState(cellNum: Int, state: Bool)
}

class SidePanelCellController: UITableViewCell {

    @IBOutlet weak var layerIdentifier : UILabel!
    @IBOutlet weak var layerSwitch : UISwitch!
    var toggleDelegate : LayerToggleDelegate?
    var stateDelegate : LayerStateDelegate?
    var cellNum: Int?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setSwitch(state: Bool) {
        layerSwitch.setOn(state, animated: false)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        //super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func layerToggle() {
            toggleDelegate?.toggleLayer(layerIdentifier.text!, state: layerSwitch.on);
            stateDelegate?.updateSavedState(cellNum!, state: layerSwitch.on)
            
        
        
    }

}
