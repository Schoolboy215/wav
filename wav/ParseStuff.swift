//
//  ParseStuff.swift
//  wav
//
//  Created by James McKay on 2/12/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import Foundation
import MapKit


class AlertAnnotation: MKPointAnnotation {
    var imageName: String!
    var kind: String!
    var descriptionText : String!
    var username : String!
    var internalID : String!
    var score : Int!
    var expires : NSDate!
}

class UserAnnotation: MKPointAnnotation {
    var imageName: String!
    var username: String!
    var internalID : String!
}


protocol nearbyAlertsHandler {
    func doneLoading()
    func addToArray(object : AnyObject)
}

protocol userSignInHandler {
    func noAccount()
    func accountFound()
    func finishAccountCreation()
    func validMigrationCode()
    func invalidMigrationCode()
    func validUsernameEntry(username : String)
    func invalidUsernameEntry()
}

protocol nearbyUserHandler {
    func doneGettingNearbyUsers()
    func addNearbyUser(object : AnyObject)
}

func addTimeToAlert(inout alert : AlertAnnotation, minutesToAdd : Int){
    var Q = PFQuery(className: "wavAlert")
    Q.getObjectInBackgroundWithId(alert.internalID) {
        (object: PFObject!, error: NSError!) -> Void in
        if error == nil {
            var components = NSDateComponents()
            components.setValue(30, forComponent: NSCalendarUnit.MinuteCalendarUnit);
            let date: NSDate = object["expires"] as! NSDate
            var expirationDate = NSCalendar.currentCalendar().dateByAddingComponents(components, toDate: date, options: NSCalendarOptions(0))
            
            let difference = Int((object["expires"] as! NSDate).timeIntervalSinceDate(NSDate()))
            
            if difference < (30*60){
                object["expires"] = expirationDate
                alert.expires = expirationDate
                object.saveInBackground()
                
                increaseUserScore(UserProfile.SharedInstance.ParseID, 1)
                increaseUserScoreByName(alert.username, 1)
            }
        }
    }
}

func returnParseObjects(delegate : nearbyAlertsHandler, locationObj : CLLocation) {
    var coord = locationObj.coordinate
    if coord.latitude != 0 {
        var Q = PFQuery(className: "wavAlert")
        Q.whereKey("location", nearGeoPoint: PFGeoPoint(latitude: coord.latitude, longitude: coord.longitude)!, withinMiles: 1)
        Q.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                // The find succeeded.
                // Do something with the found objects
                for object in objects {
                    delegate.addToArray(object)
                }
            } else {
            // Log details of the failure
            //NSLog("Error: %@ %@", error, error.userInfo!)
            }
            delegate.doneLoading()
        }
    }
}

func getNearbyUsers(delegate : nearbyUserHandler, locationObj : CLLocation) {
    var coord = locationObj.coordinate
    var Q = PFQuery(className: "wavUsers")
    Q.whereKey("lastKnownLoc", nearGeoPoint: PFGeoPoint(latitude: coord.latitude, longitude: coord.longitude)!, withinMiles: 1)
    Q.findObjectsInBackgroundWithBlock {
        (objects: [AnyObject]!, error: NSError!) -> Void in
        if error == nil {
            for object in objects {
                delegate.addNearbyUser(object)
            }
            delegate.doneGettingNearbyUsers()
        }
    }
}

func checkDeviceID(delegate: userSignInHandler, deviceId: String) {

    var Q = PFQuery(className: "wavUsers")
    Q.whereKey("deviceID", equalTo: deviceId)
    Q.findObjectsInBackgroundWithBlock {
        (objects: [AnyObject]!, error: NSError!) -> Void in
        if error == nil {
            if objects.count == 0 {
                delegate.noAccount()
            }
            else {
                for object in objects {
                    UserProfile.SharedInstance.Username = object["userName"] as! String
                    UserProfile.SharedInstance.ParseID = object.objectId as String
                    UserProfile.SharedInstance.VisibilityState = object["visibilityState"] as! Bool
                    UserProfile.SharedInstance.Score = object["userScore"] as! Int
                    getExistingMigrationCode()
                    delegate.accountFound()
                }
            }
            
        }
        else {

        }
    }
}

func createNewAccount(delegate: userSignInHandler, deviceId: String, username: String) {
    var account = PFObject(className: "wavUsers")
    account["deviceID"] = deviceId
    account["userName"] = username
    account["visibilityState"] = true
    account["userScore"] = 0
    account["createdAlertCount"] = 0
    account["validatedAlertCount"] = 0
    account.saveInBackground()
    delegate.finishAccountCreation()
}

func setInvisState() {

    let accountID = UserProfile.SharedInstance.ParseID
    let accountVisibilityState = UserProfile.SharedInstance.VisibilityState
    var Q = PFQuery(className: "wavUsers")
    Q.getObjectInBackgroundWithId(accountID) {
        (object: PFObject!, error: NSError!) -> Void in
        if error == nil {
            object["visibilityState"] = accountVisibilityState
            object.saveInBackground()
        }
    }
}

//This function is called to increment the userscore on the server side, then a sync call is made to update the local record
func increaseUserScore(parseID : String, scoreInc: Int) {
    //let accountID = UserProfile.SharedInstance.ParseID
    var Q = PFQuery(className: "wavUsers")
    Q.getObjectInBackgroundWithId(parseID) {
        (object: PFObject!, error: NSError!) -> Void in
        if error == nil {
            object["userScore"] = (object["userScore"] as! Int) + scoreInc
            object.saveInBackground()
            syncProfile()
        }
    }
}

func updateUserLocation(locationObj: CLLocation) {
    var coord = locationObj.coordinate
    var Q = PFQuery(className: "wavUsers")
    Q.getObjectInBackgroundWithId(UserProfile.SharedInstance.ParseID) {
        (object: PFObject!, error: NSError!) -> Void in
        if error == nil {
            object["lastKnownLoc"] = PFGeoPoint(latitude: coord.latitude, longitude: coord.longitude)
            object.saveInBackground()
            syncProfile()
        }
    }

}

func increaseUserScoreByName(username : String, scoreInc: Int) {
    //let accountID = UserProfile.SharedInstance.ParseID
    var Q = PFQuery(className: "wavUsers")
    Q.whereKey("userName", equalTo: username)
    Q.findObjectsInBackgroundWithBlock {
        (objects: [AnyObject]!, error: NSError!) -> Void in
        if error == nil {
            // The find succeeded.
            // Do something with the found objects
            for object in objects {
                var id = object.objectId
                var R = PFQuery(className: "wavUsers")
                R.getObjectInBackgroundWithId(id) {
                    (object: PFObject!, error: NSError!) -> Void in
                    if error == nil {
                        object["userScore"] = (object["userScore"] as! Int) + scoreInc
                        object.saveInBackground()
                    }
                }
            }
        } else {
            // Log details of the failure
            //NSLog("Error: %@ %@", error, error.userInfo!)
        }
    }
}


//Called every time an update to the DB has been made, to retrieve the most up-to-date data from the server.
func syncProfile() {
    let accountID = UserProfile.SharedInstance.ParseID
    var Q = PFQuery(className: "wavUsers")
    Q.getObjectInBackgroundWithId(accountID) {
        (object: PFObject!, error: NSError!) -> Void in
        if error == nil {
            UserProfile.SharedInstance.Username = object["userName"] as! String
            UserProfile.SharedInstance.VisibilityState = object["visibilityState"] as! Bool
            UserProfile.SharedInstance.Score = object["userScore"] as! Int
            UserProfile.SharedInstance.ValidatedAlerts = object["validatedAlertCount"] as! Int
            UserProfile.SharedInstance.CreatedAlerts = object["createdAlertCount"] as! Int
        }
    }
    //getExistingMigrationCode()
}

func increaseCreatedAlertCount() {
    let accountID = UserProfile.SharedInstance.ParseID
    var Q = PFQuery(className: "wavUsers")
    Q.getObjectInBackgroundWithId(accountID) {
        (object: PFObject!, error: NSError!) -> Void in
        if error == nil {
            object["createdAlertCount"] = (object["createdAlertCount"] as! Int) + 1
            object.saveInBackground()
            syncProfile()
        }
    }
}

func increaseValidatedAlertCount() {
    let accountID = UserProfile.SharedInstance.ParseID
    var Q = PFQuery(className: "wavUsers")
    Q.getObjectInBackgroundWithId(accountID) {
        (object: PFObject!, error: NSError!) -> Void in
        if error == nil {
            object["validatedAlertCount"] = (object["validatedAlertCount"] as! Int) + 1
            object.saveInBackground()
            syncProfile()
        }
    }
}

//Client generates a migration code, sends it to the server with information about the device/account
func migrationCodeCreation(code: String) {
    var migrationEntry = PFObject(className: "migration")
    migrationEntry["userName"] = UserProfile.SharedInstance.Username
    migrationEntry["migrationCode"] = code
    migrationEntry["userID"] = UserProfile.SharedInstance.ParseID
    migrationEntry["claimed"] = false
    migrationEntry.saveInBackground()
}

func migrationCodeCheck(delegate: userSignInHandler, code: String) {
    PFCloud.callFunctionInBackground("checkMigrationCode", withParameters: ["migrationCode" : code, "deviceID" : UserProfile.SharedInstance.DeviceID]) {
        ( response : AnyObject!, error : NSError!) -> Void in
        if error == nil {
            delegate.validMigrationCode()
        }
        else {
            delegate.invalidMigrationCode()
        }
    }
}

func getExistingMigrationCode() {
    PFCloud.callFunctionInBackground("getMigrationCode", withParameters: ["userName" : UserProfile.SharedInstance.Username]) {
        ( response : AnyObject!,  error : NSError!) -> Void in
        if error == nil {
            if let obj = response as? PFObject {
                UserProfile.SharedInstance.MigrationCode = obj["migrationCode"] as! String
            }
            else {
                UserProfile.SharedInstance.MigrationCode = ""
            }
        }
    }
}

func checkExistingUsername(username : String, delegate : userSignInHandler) {
    PFCloud.callFunctionInBackground("checkExistingUsername", withParameters: ["userName" : username]) {
        ( response : AnyObject!, error : NSError!) -> Void in
        if error == nil {
            delegate.validUsernameEntry(username)
        }
        else {
            delegate.invalidUsernameEntry()
        }
    }
}