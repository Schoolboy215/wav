
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response) {
  response.success("Hello world!");
});

Parse.Cloud.define("increaseScore", function(request, response) {
  var query = new Parse.Query("wavUsers");
  query.equalTo("objectId", request.params.parseID);
  query.find({
    success: function(results) {
      var score = results[0].get("userScore");
      score += parseInt(request.params.scoreInc);
      results[0].set("userScore", score);
      results[0].save();
      response.success("updated score");
    }
  });
});

Parse.Cloud.define("incrementCreatedAlertCount", function(request, response) {
  var query = new Parse.Query("wavUsers");
  query.equalTo("objectId", request.params.parseID);
  query.find({
    success: function(results) {
      var alertCount = results[0].get("createdAlertCount");
      alertCount=alertCount + 1;
      results[0].set("createdAlertCount", alertCount);
      results[0].save();
      response.success("Incremented created alert count");
    }
  });
});


Parse.Cloud.define("incrementValidatedAlertCount", function(request, response) {
  var query = new Parse.Query("wavUsers");
  query.equalTo("objectId", request.params.parseID);
  query.find({
    success: function(results) {
      var alertCount = results[0].get("validatedAlertCount");
      alertCount++;
      results[0].set("validatedAlertCount", alertCount);
      results[0].save();
      response.success("Incremented validated alert count");
    }
  });
});


Parse.Cloud.define("checkMigrationCode", function(request, response) {
  var migrationQuery = new Parse.Query("migration");
  migrationQuery.equalTo("migrationCode", request.params.migrationCode);
  migrationQuery.find({
    success: function(migrateResults) {
      if(migrateResults.length > 0) {
        if(migrateResults[0].get("claimed")) {
          return response.error("Migration code already claimed");
        }
      }
      else {
        return response.error("No code found");
      }
      var userQuery = new Parse.Query("wavUsers");
      userQuery.equalTo("userName", migrateResults[0].get("userName"));
      userQuery.find({
        success: function(userResults) {
          if(userResults.length > 0) {
            userResults[0].set("deviceID", request.params.deviceID);
	    userResults[0].save();
            migrateResults[0].set("claimed", true);
            migrateResults[0].save();
            response.success("profile migrated");
          }
          else {
            return response.error("Problem");
          } 
        },
        error: function(userResults) {
          return response.error("Could not set deviceID");
        }
      });
      
    },
    error: function(migrateResults) {
      response.error("Invalid code");
    }
  });
});

Parse.Cloud.define("getMigrationCode", function(request, response) {
  var migrationQuery = new Parse.Query("migration");
  migrationQuery.equalTo("userName", request.params.userName);
  migrationQuery.find({
    success: function (migrateResults) {
      if(migrateResults.length > 0) {
        if(migrateResults[0].get("claimed") == false) {
      	  return response.success(migrateResults[0]);
        }
        else {
          return response.error("Code claimed");
        }
      }
      else {
        return response.error("no Code found");
      }
    },
    error: function (migrateResults) {
      response.error("no code found");
    }
  });
});

Parse.Cloud.define("checkExistingUsername", function(request, response) {
  var userQuery = new Parse.Query("wavUsers");
  userQuery.equalTo("userName", request.params.userName);
  userQuery.find({
    success: function (results) {
      if(results.length > 0) {
        return response.error("Username already taken");
      }
      else {
        return response.success("Username available");
      }
    },
    error: function (results) {
      return response.error("UserName already taken");
    }
  });
});

Parse.Cloud.job("emptyMigrationTable", function(request, status) {
  var date = new Date();
  date.setDate(date.getHour()-1);

  var query = new Parse.Query("migration");
  query.lessThan("createdAt", date);
  query.equalTo("claimed", true);

  query.find({
    success: function(results) {
      var i = 0;
      var len = results.length;
      for (i = 0; i < len; i++) {
        var result = results[i];
        result.destroy({});
      }
      status.success("Deleted successfully.");
    },
    error: function(error) {
      status.error("Error deleting.");
    }
  });
});
    