//
//  MapViewTests.swift
//  wav
//
//  Created by David Goehring on 3/17/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import UIKit
import XCTest
import MapKit
import wav

class MapViewTests: XCTestCase {

    var storyboard: UIStoryboard = UIStoryboard()
    var mapView: MapController = MapController()
    var window:UIWindow = UIWindow()
    
    override func setUp() {
        super.setUp()
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        storyboard = UIStoryboard(name: "Main", bundle: NSBundle(forClass: self.dynamicType))
        mapView = storyboard.instantiateViewControllerWithIdentifier("MapViewController") as MapController
        window.rootViewController = mapView
        mapView.loadView()
        mapView.viewDidLoad()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }
    func testMapViewLoaded() {
        XCTAssertNotNil(mapView.view, "View did not load")
    }
    func testInitialStandardMapView() {
        XCTAssert(mapView.mapView.mapType == MKMapType.Standard, "Map should initialize as standard view")
    }
    func testToggleSatelliteLayerOn() {
        mapView.toggleLayer("Satellite",state: true)
        XCTAssert(mapView.mapView.mapType == MKMapType.Satellite, "Map should be Satellite mode")
    }
    func testToggleSatelliteLayerOff() {
        mapView.toggleLayer("Satellite", state: false)
        XCTAssert(mapView.mapView.mapType == MKMapType.Standard, "Map should be in Standard mode")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }

}
