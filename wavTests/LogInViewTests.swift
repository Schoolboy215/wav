//
//  LogInViewTests.swift
//  wav
//
//  Created by David Goehring on 3/14/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import UIKit
import XCTest
import wav

class LogInViewTests: XCTestCase {

    var storyboard: UIStoryboard = UIStoryboard()
    var logInView:LogInController = LogInController()
    var window:UIWindow = UIWindow()
    
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        storyboard = UIStoryboard(name: "Main", bundle: NSBundle(forClass: self.dynamicType))
        logInView = storyboard.instantiateViewControllerWithIdentifier("LogInView") as! LogInController
        window.rootViewController = logInView
        logInView.loadView()
        logInView.viewDidLoad()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }

}
