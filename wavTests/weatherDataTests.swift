//
//  weatherDataTests.swift
//  wav
//
//  Created by Kaylie DeNuto on 4/13/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import UIKit
import XCTest
import wav

class weatherDataTests: XCTestCase {

    let mapView: MapController = MapController()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testWeatherCall() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 11.24
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        XCTAssert(resultsString != nil, "Pass")
    }

    func testWindDirectionNorthUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 11.24
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph N \n Pressure: \(pressure) hpa"
        
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionNorthNorthEastLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 11.25
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph NNE \n Pressure: \(pressure) hpa"
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionNorthNorthEastUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 33.74
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph NNE \n Pressure: \(pressure) hpa"
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionNorthEastLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 33.75
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph NE \n Pressure: \(pressure) hpa"
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionNorthEastUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 56.24
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph NE \n Pressure: \(pressure) hpa"
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionEastNorthEastLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 56.25
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph ENE \n Pressure: \(pressure) hpa"
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionEastNorthEastUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 78.74
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph ENE \n Pressure: \(pressure) hpa"
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionEastLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 78.75
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph E \n Pressure: \(pressure) hpa"
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionEastUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 101.24
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph E \n Pressure: \(pressure) hpa"
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionEastSouthEastLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 101.25
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph ESE \n Pressure: \(pressure) hpa"
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionEastSouthEastUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 123.74
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph ESE \n Pressure: \(pressure) hpa"
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionSouthEastLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 123.75
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph SE \n Pressure: \(pressure) hpa"
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionSouthEastUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 146.24
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph SE \n Pressure: \(pressure) hpa"
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionSouthSouthEastLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 146.25
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph SSE \n Pressure: \(pressure) hpa"
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionSouthSouthEastUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 168.74
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph SSE \n Pressure: \(pressure) hpa"
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionSouthLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 168.75
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph S \n Pressure: \(pressure) hpa"
        
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionSouthUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 191.24
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph S \n Pressure: \(pressure) hpa"
        NSLog(resultsString)
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionSouthSouthWestLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 191.25
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph SSW \n Pressure: \(pressure) hpa"
        
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionSouthSouthWestUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 213.74
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph SSW \n Pressure: \(pressure) hpa"
        NSLog(resultsString)
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionSouthWestLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 213.75
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph SW \n Pressure: \(pressure) hpa"
        
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionSouthWestUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 236.24
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph SW \n Pressure: \(pressure) hpa"
        NSLog(resultsString)
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionWestSouthWestLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 236.25
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph WSW \n Pressure: \(pressure) hpa"
        
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionWestSouthWestUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 258.74
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph WSW \n Pressure: \(pressure) hpa"
        NSLog(resultsString)
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionWestLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 258.75
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph W \n Pressure: \(pressure) hpa"
        
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionWestUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 281.24
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph W \n Pressure: \(pressure) hpa"
        
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionWestNorthWestLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 281.25
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph WNW \n Pressure: \(pressure) hpa"
        
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionWestNorthWestUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 303.74
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph WNW \n Pressure: \(pressure) hpa"
        
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionNorthWestLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 303.75
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph NW \n Pressure: \(pressure) hpa"
        
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionNorthWestUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 326.24
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph NW \n Pressure: \(pressure) hpa"
        
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionNorthNorthWestLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 326.25
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph NNW \n Pressure: \(pressure) hpa"
        
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionNorthNorthWestUpperBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 348.74
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph NNW \n Pressure: \(pressure) hpa"
        
        XCTAssert(resultsString == testString, "Pass")
    }
    
    func testWindDirectionNorthLowerBound() {
        var temp: Float = 100
        var wind: Float = 3
        var windDegrees: Float = 348.75
        var pressure: Float = 3
        
        var resultsString = mapView.updateWeatherLabel(temp, wind: wind, pressure: pressure, windDegrees: windDegrees)
        var testString = "Temperature: \(temp) F \n Wind: \(wind) mph N \n Pressure: \(pressure) hpa"
        
        XCTAssert(resultsString == testString, "Pass")
    }
}
