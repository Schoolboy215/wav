//
//  ParseCloudCodeTests.swift
//  wav
//
//  Created by David Goehring on 4/10/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

import UIKit
import XCTest
import wav

class ParseCloudCodeTests: XCTestCase {

    override func setUp() {
        super.setUp()
        UserProfile.SharedInstance.DeviceID = UIDevice.currentDevice().identifierForVendor.UUIDString
        UserProfile.SharedInstance.ParseID = "4XNEU6QMTH"
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }
    func testScoreInc() {
        var scoreInc = 1
        var responseMessage : String = ""
        
        PFCloud.callFunctionInBackground("increaseScore", withParameters: ["parseID" : UserProfile.SharedInstance.ParseID, "scoreInc" : scoreInc]) {
            ( response : AnyObject!,  error : NSError!) -> Void in
            if error == nil {
                NSLog(response as! String)
                responseMessage = response as! String
                
            }
        }
        XCTAssert(responseMessage == "updated score", "score failed")
    }
    func testCreatedAlertInc() {
        var responseMessage : String = ""
        PFCloud.callFunctionInBackground("incrementCreatedAlertCount", withParameters: ["parseID" : UserProfile.SharedInstance.ParseID]) {
            ( response : AnyObject!, error : NSError!) -> Void in
            if error == nil {
                println(response)
                responseMessage = response as! String
                
            }
        }
        XCTAssert(responseMessage == "Incremented created alert count", "created alert count failed")
    }
    func testValidatedAlertInc() {
        var responseMessage : String = ""
        PFCloud.callFunctionInBackground("incrementValidatedAlertCount", withParameters: ["parseID" : UserProfile.SharedInstance.ParseID]) {
            ( response : AnyObject!, error : NSError!) -> Void in
            if error == nil {
                println(response)
                responseMessage = response as! String
                
            }
        }
        XCTAssert(responseMessage == "Incremented validated alert count", "validated alert count failed")
    }
    func testMigrationCheck() {
        var responseMessage : String = ""
        PFCloud.callFunctionInBackground("checkMigrationCode", withParameters: ["migrationCode" : "abcdef", "deviceID" : UserProfile.SharedInstance.DeviceID]) {
            ( response : AnyObject!, error: NSError!) -> Void in
            if error == nil {
                println(response)
                responseMessage = response as! String
                
            }
        }
        XCTAssert(responseMessage == "profile migrated", "error migrating")
    }
    func testMigrationCheckFail() {
        var responseMessage : String = ""
        PFCloud.callFunctionInBackground("checkMigrationCode", withParameters: ["migrationCode" : "ffffff", "deviceID" : UserProfile.SharedInstance.DeviceID]) {
            ( response : AnyObject!, error: NSError!) -> Void in
            if error == nil {
                println(response)
                responseMessage = response as! String
                
            }
        }
        XCTAssert(responseMessage == "Invalid code", "code was successful")
    }
    func testMigrationCheckAlreadyClaimed() {
        var responseMessage : String = ""
        PFCloud.callFunctionInBackground("checkMigrationCode", withParameters: ["migrationCode" : "abcdef", "deviceID" : UserProfile.SharedInstance.DeviceID]) {
            ( response : AnyObject!, error: NSError!) -> Void in
            if error == nil {
                println(response)
                responseMessage = response as! String
                
            }
        }
        XCTAssert(responseMessage == "Migration code already claimed", "code was successful")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }

}
