//
//  LogInController.swift
//  wav
//
//  Created by David Goehring on 2/18/15.
//  Copyright (c) 2015 James McKay. All rights reserved.
//

//Edited on 3/25/15
//David Goehring

import UIKit

private let UserProfileSharedInstance = UserProfile()

class UserProfile {
    class var SharedInstance: UserProfile {
        return UserProfileSharedInstance
    }
    init() {
        Username = ""
        DeviceID = ""
        ParseID = ""
        VisibilityState = true
        Score = 0
        CreatedAlerts = 0
        ValidatedAlerts = 0
        MigrationCode = ""
    }
    var Username : String
    var DeviceID : String
    var ParseID : String
    var VisibilityState : Bool
    var Score: Int
    var CreatedAlerts: Int
    var ValidatedAlerts: Int
    var MigrationCode: String
    
}

class LogInController: UIViewController, SignUpDelegate, userSignInHandler, CLLocationManagerDelegate {
    
    var window: UIWindow?
    
    let deviceID = UIDevice.currentDevice().identifierForVendor.UUIDString
    var usernameText : String!
    
    var locManager : CLLocationManager!
    
    @IBOutlet weak var busyAnimation : UIActivityIndicatorView!
    
    @IBOutlet var newUsernameTextField : UITextField!
    @IBOutlet var migrationTextField : UITextField!
    
    var noAccountAlertController : UIAlertController!
    var newAccountAlertController : UIAlertController!
    var usernameErrorAlertController : UIAlertController!
    var successfulRegistrationAlertController : UIAlertController!
    var migrationInputAlertController : UIAlertController!
    var migrationErrorAlertController : UIAlertController!
    var migrationSuccessAlertController : UIAlertController!
    var check : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        check = false
        locManager = CLLocationManager()
        locManager.delegate = self
        //locationManager = CLLocationManager()
        locManager.requestAlwaysAuthorization()
        
        locManager.startUpdatingLocation()
       // locationManager.startUpdatingLocation()
       // locationManager.desiredAccuracy = kCLLocationAccuracyBest

        
        
        buildAlertControllers()
        UserProfile.SharedInstance.DeviceID = deviceID
        busyAnimation.startAnimating()
    }
    /*
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if status == CLAuthorizationStatus.AuthorizedAlways || status == CLAuthorizationStatus.AuthorizedWhenInUse {
            manager.startUpdatingLocation()
            manager.desiredAccuracy = kCLLocationAccuracyBest
            checkDeviceID(self, deviceID)
        }
    }*/
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        if(!check) {
            check = true
            manager.startUpdatingLocation()
            manager.desiredAccuracy = kCLLocationAccuracyBest
            checkDeviceID(self, deviceID)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Force Portrait Rotation, Disables Rotation
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.Portrait.rawValue)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "SignUpSegue") {
            let signUpView = segue.destinationViewController as! SignUpController
            signUpView.delegate = self
        }
        else if(segue.identifier == "LogInSegue") {
            let mapView = segue.destinationViewController as! MapController
            mapView.locationManager = locManager
            window = UIWindow(frame: UIScreen.mainScreen().bounds)
            
            let containerViewController = ContainerViewController()
            
            window!.rootViewController = containerViewController
            window!.makeKeyAndVisible()
        }
    }
    
    func done(child: SignUpController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func buildAlertControllers() {
        
        //Initial alert when no account is found tied to the device
        noAccountAlertController = UIAlertController(title: "No Account Found On This Device", message: "Would you like to create a new account, or migrate an account from an existing device?", preferredStyle: UIAlertControllerStyle.Alert)
        let createAccountActionHandler = { (action:UIAlertAction!) -> Void in
            self.presentViewController(self.newAccountAlertController, animated: true, completion: nil)
            //createNewAccount(self.deviceID)
            //NSLog("Created New Account")
        }
        let migrateAccountActionHandler = { (action:UIAlertAction!) -> Void in
            //Make view here for account migration
            NSLog("Account migration view")
            self.presentViewController(self.migrationInputAlertController, animated: true, completion: nil)
        }
        let createAccountAction = UIAlertAction(title: "Create New Account", style: .Default, handler: createAccountActionHandler)
        let migrateAccountAction = UIAlertAction(title: "Migrate Account", style: .Default, handler: migrateAccountActionHandler)
        noAccountAlertController.addAction(createAccountAction)
        noAccountAlertController.addAction(migrateAccountAction)
        
        
        //Alert presented for requesting new account generation
        newAccountAlertController = UIAlertController(title: "Create New Account", message: "Please enter a username for your new account.", preferredStyle: UIAlertControllerStyle.Alert)
        newAccountAlertController.addTextFieldWithConfigurationHandler(self.addUsernameTextField)
        
        let submitHandler = { (action:UIAlertAction!) -> Void in
            let regex = NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: nil, error: nil)!
            let text : NSString = self.newUsernameTextField.text
            if (text.length < 3) {
                self.usernameErrorAlertController.message = "Too few characters in username \n(at least 3)"
                self.presentViewController(self.usernameErrorAlertController, animated: true, completion: nil)
                NSLog("String too short")
            }
            else if text.length > 16 {
                self.usernameErrorAlertController.message = "Too many characters in username \n(less than 17)"
                self.presentViewController(self.usernameErrorAlertController, animated: true, completion: nil)
                NSLog("String too long")
            }
            else if (regex.firstMatchInString(text as String, options: nil, range: NSMakeRange(0, text.length)) != nil) {
                self.usernameErrorAlertController.message = "Invalid characters in username \n(A-Z, a-z, 0-9)"
                self.presentViewController(self.usernameErrorAlertController, animated: true, completion: nil)
                NSLog("String contains wrong chars")
            }
            else {
                checkExistingUsername(text as String, self)
            }
        }
        let cancelHandler = { (action:UIAlertAction!) -> Void in
            NSLog("Cancelling account creation")
            self.presentViewController(self.noAccountAlertController, animated: true, completion: nil)
        }
        let cancelNewAccount = UIAlertAction(title: "Cancel", style: .Default, handler: cancelHandler)
        let submitNewAccount = UIAlertAction(title: "Submit", style: .Default, handler: submitHandler)
        newAccountAlertController.addAction(cancelNewAccount)
        newAccountAlertController.addAction(submitNewAccount)

        
        //Alert presented in the case of an invalid username entry
        usernameErrorAlertController = UIAlertController(title: "Username Error", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        let usernameErrorAcceptHandler = { (action:UIAlertAction!) -> Void in
            self.presentViewController(self.newAccountAlertController, animated: true, completion: nil)
        }
        let usernameErrorAccept = UIAlertAction(title: "Ok", style: .Default, handler: usernameErrorAcceptHandler)
        usernameErrorAlertController.addAction(usernameErrorAccept)
        
        //Alert presented if user signup was successful
        successfulRegistrationAlertController = UIAlertController(title: "Account Creation Successful!", message: "Wav will automatically log you in on this device each time you run the app.\nTo migrate an existing account, visit the User Settings page.", preferredStyle: UIAlertControllerStyle.Alert)
        let successfulAcceptHandler = { (action:UIAlertAction!) -> Void in
            createNewAccount(self, self.deviceID, self.usernameText)
        }
        let successfulAccept = UIAlertAction(title: "Ok", style: .Default, handler: successfulAcceptHandler)
        successfulRegistrationAlertController.addAction(successfulAccept)
        
        //Alert presented if migrating account option was selected
        migrationInputAlertController = UIAlertController(title: "Migrate an Account to This Device", message: "Please enter the migration code given to you on the device that currently owns the account.", preferredStyle: UIAlertControllerStyle.Alert)
        migrationInputAlertController.addTextFieldWithConfigurationHandler(self.addMigrationTextField)
        let cancelMigrationHandler = { (action:UIAlertAction!) -> Void in
            self.migrationTextField.text = ""
            self.presentViewController(self.noAccountAlertController, animated: true, completion: nil)
        }
        let acceptMigrationHandler = { (action:UIAlertAction!) -> Void in
            migrationCodeCheck(self, self.migrationTextField.text)
        }
        let cancelMigration = UIAlertAction(title: "Cancel", style: .Default, handler: cancelMigrationHandler)
        let acceptMigration = UIAlertAction(title: "Submit", style: .Default, handler: acceptMigrationHandler)
        migrationInputAlertController.addAction(cancelMigration)
        migrationInputAlertController.addAction(acceptMigration)
        
        migrationErrorAlertController = UIAlertController(title: "Migration Error", message: "Code is not valid. The code may have expired. Please check the migration code or generate a new code on the current device.", preferredStyle: UIAlertControllerStyle.Alert)
        let acceptMigrationErrorHandler = { (action:UIAlertAction!) -> Void in
            self.presentViewController(self.migrationInputAlertController, animated: true, completion: nil)
        }
        let acceptMigrationError = UIAlertAction(title: "Ok", style: .Default, handler: acceptMigrationErrorHandler)
        migrationErrorAlertController.addAction(acceptMigrationError)
        
        migrationSuccessAlertController = UIAlertController(title: "Migration Success", message: "Migration was successful! You will be logged in as your migrated account.", preferredStyle: UIAlertControllerStyle.Alert)
        let acceptMigrationSuccessHandler = { (action:UIAlertAction!) -> Void in
            checkDeviceID(self, UserProfile.SharedInstance.DeviceID)
        }
        let acceptMigrationSuccess = UIAlertAction(title: "Ok", style: .Default, handler: acceptMigrationSuccessHandler)
        migrationSuccessAlertController.addAction(acceptMigrationSuccess)
        
    }
    
    func addUsernameTextField(textField : UITextField!) {
        textField.placeholder = "username"
        self.newUsernameTextField = textField
    }
    
    func addMigrationTextField(textField : UITextField!) {
        textField.placeholder = "Enter code"
        self.migrationTextField = textField
    }
    
    func finishAccountCreation() {
        self.performSegueWithIdentifier("LogInSegue", sender: nil)
    }
    
    //Delegate call if no accounts are tied to device
    func noAccount() {
        presentViewController(noAccountAlertController, animated: true, completion: nil)
    }
    
    func validMigrationCode() {
        self.presentViewController(self.migrationSuccessAlertController, animated : true, completion: nil)
    }
    func invalidMigrationCode() {
        self.presentViewController(self.migrationErrorAlertController, animated : true, completion: nil)
    }
    
    //Delegate call if account is found tied to device
    func accountFound() {
        /*
        PFCloud.callFunctionInBackground("increaseScore", withParameters: ["parseID" : UserProfile.SharedInstance.ParseID, "scoreInc" : "5"]) {
            ( response : AnyObject!,  error : NSError!) -> Void in
            if error == nil {
                println(response)
            }
        }*/
        busyAnimation.stopAnimating()
        self.performSegueWithIdentifier("LogInSegue", sender: nil)
    }
    
    func validUsernameEntry(username : String) {
        self.usernameText = username
        self.presentViewController(self.successfulRegistrationAlertController, animated: true, completion: nil)
    }
    func invalidUsernameEntry() {
        self.usernameErrorAlertController.message = "Username already taken"
        self.presentViewController(self.usernameErrorAlertController, animated: true, completion: nil)

    }
}
